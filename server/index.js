const express=require('express')
const app=express()
const bodyParser=require('body-parser')
const sqlite3 = require('sqlite3').verbose();
var cors=require('cors');
const tabletojson = require('tabletojson').Tabletojson;

let db = new sqlite3.Database('../db/db.db', sqlite3.OPEN_READWRITE, (err) => {
    if (err) {
      console.error(err.message);
    }
    console.log('Connected to db.');
  });

app.use(bodyParser.urlencoded({extended: true}));
app.use(express.json());
app.use(cors());

app.get('/naplatne', (req,res)=>{
    const sqlSelect="select * from naplatne";
    db.all(sqlSelect,[],(err,result)=>{
        res.send(result);
    });
})
app.get('/tablica/:ulazna',(req,res)=>{
  const ulazna = req.params.ulazna;
  const requestUrl = new URL ("https://www.hak.hr/handlers/toll/toll.ashx")
  requestUrl.searchParams.append("o","toll");
  requestUrl.searchParams.append("tollid", ulazna);
  
  tabletojson.convertUrl(
      requestUrl.href, {headings: ['A','B','C','D','E','F']} ,function(tablesAsJson) {
          res.send(tablesAsJson);
      })
})
app.get('/geodatau/:ulazna',(req,res)=>{
  const ulazna=req.params.ulazna;
  const sqlSelect="select geo_sirina,geo_duzina from naplatne where ID=?"
  db.all(sqlSelect,[ulazna],(err,result)=>{
    res.send(result);
  })
})
app.get('/geodatai/:izlazna',(req,res)=>{
  const izlazna=req.params.izlazna;
  const sqlSelect="select geo_sirina,geo_duzina from naplatne where naziv=?"
  db.all(sqlSelect,[izlazna],(err,result)=>{
    res.send(result);
  })
})
app.listen(3001,()=>{
    console.log("port 3001");
});