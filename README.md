# Izračun cijena cestarina

Aplikacija za izračun cijene cestarina je zamišljena kao web aplikacija koja služi za izračun cestarine između naplatnih postaja. Korisnici bi u aplikaciju unosili ulaznu i izlaznu naplatnu postaju, te odabrali kategoriju vozila. Zavisno od udaljenosti postaja te kategoriji vozila korisniku bi se ispisala cijena cestarine koju je potrebno platiti. Nakon izračuna cijene korisniku bi se pojavila i karta sa označenim putem te bi dobio informacije  o udaljenosti i trajanju putovanja. 

Jedan od glavnih razloga odabira ove teme je to što je ovaj proces trenutno neučinkovit. Korisnik do informacije dolazi pretraživanjem tablica koje su teško pregledne. Cilj ove naše aplikacije bi bio olakšati dolazak do informacija o cijenama cestarina.

# Instalacija aplikacije
Ako u sustavu nije instaliran Node.js, potrebno ga je preuzeti sa stranice https://nodejs.org/en/ i instalirati
## Instalacija na serverskoj strani

#### 1. korak
```
cd server
```
#### 2. korak
```
npm install axios express cors body-parser sqlite3 tabletojson
```

#### Pokrenuti kod
```
node index.js
```
## Instalacija na klijentskoj strani
#### 1. korak
Otvoriti novi terminal i napisati
```
cd client
```
#### 2. korak
```
npm install axios cypress react react-google-maps react-select
```
#### Pokrenuti kod
```
npm start
```
### Otvaranje aplikacije u pregledniku
```
http://localhost:3000/ 
```
