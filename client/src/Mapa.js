import React from "react";
import "./Mapa.css";
import PropTypes from "prop-types";
import {
    withGoogleMap,
    GoogleMap,
    DirectionsRenderer,
    Marker
} from "react-google-maps";
import gasstation from './icons/gasstation.png';
const google = window.google;
export default class Mapa extends React.PureComponent {
    constructor(props){
        super(props);
        this.state={
            directions:null,
            distance:null,
            time:null,
            markeri:[
                {position: {lat: 45.045194258922265, lng: 19.000497069086645}},
                {position: {lat: 45.103823483188336, lng: 18.700563731862513}},
                {position: {lat: 45.11134819108625, lng: 18.52043724543913}},
                {position: {lat: 45.179757113636455, lng: 18.005784740500108}},
                {position: {lat: 45.21711531548199, lng: 17.515280543652697}},
                {position: {lat: 45.321626855292976, lng: 17.02331677930993}},
                {position: {lat: 45.63835859841799, lng: 16.513226542757423}},
                {position: {lat: 45.32716482662511, lng: 14.657099683273849}},
                {position: {lat: 45.27698070374396, lng: 14.570811399995383}},
                {position: {lat: 45.18582454481328, lng: 14.683727953986622}},
                {position: {lat: 45.12559766761528, lng: 14.792711411631368}},
                {position: {lat: 44.46268593785913, lng: 15.601377159483746}},
                {position: {lat: 42.84067437693927, lng: 17.74529411689325}},
                {position: {lat: 45.318670508625104, lng: 14.172355898141621}},
                {position: {lat: 45.410231266429186, lng: 13.969181809792019}},
                {position: {lat: 45.2390993059656, lng: 13.717455280878575}},
                {position: {lat: 45.36351569235475, lng: 14.490959376435333}},
                {position: {lat: 45.45753620305381, lng: 15.490962084441216}},
                {position: {lat: 44.867967073257674, lng: 15.249985411410446}},
                {position: {lat: 44.46530738399318, lng: 15.603183906020659}},
                {position: {lat: 45.74638833956842, lng: 15.881209526953308}},
                {position: {lat: 45.759264281914845, lng: 15.901379994013196}},
                {position: {lat: 45.66362865905157, lng: 15.712418648702466}},
                {position: {lat: 45.560767837712454, lng: 15.60788024710182}},
                {position: {lat: 45.51333465886645, lng: 15.547110551626393}},
                {position: {lat: 45.501033851090064, lng: 15.54552259234341}},
                {position: {lat: 45.45558536092323, lng: 15.377229250048368}},
                {position: {lat: 45.37385274639572, lng: 15.0766227349532}},
                {position: {lat: 45.38259415636482, lng: 14.936997951839768}},
                {position: {lat: 45.391600520451135, lng: 14.79103973127476}},
                {position: {lat: 45.30527100439523, lng: 14.71337271435603}},
                {position: {lat: 45.322047505310216, lng: 14.624690244405636}},
                {position: {lat: 45.310072054437306, lng: 15.279106704184818}},
                {position: {lat: 45.23266624111632, lng: 15.258256562204947}},
                {position: {lat: 45.00750295784215, lng: 15.128856263737882}},
                {position: {lat: 44.89370712723636, lng: 15.184327126840886}},
                {position: {lat: 44.751822058567136, lng: 15.373015328772746}}
              ],
              lat_ulaz: this.props.ulaz_lat,
              lng_ulaz: this.props.ulaz_lng,
              lat_izlaz: this.props.izlaz_lat,
              lng_izlaz: this.props.izlaz_lng
        }
    }
    componentDidMount(){
          const directionsService = new google.maps.DirectionsService();
          const origin = new google.maps.LatLng(this.state.lat_ulaz,this.state.lng_ulaz);
          const destination = new google.maps.LatLng(this.state.lat_izlaz,this.state.lng_izlaz);
          directionsService.route(
            {
                origin: origin,
                destination: destination,
                travelMode: google.maps.TravelMode.DRIVING,
                drivingOptions: {
                  departureTime: new Date(),
                  trafficModel: 'pessimistic'
                },
                unitSystem: google.maps.UnitSystem.IMPERIAL
            },
            (result, status) => {
                if (status === google.maps.DirectionsStatus.OK) {
                    console.log(result)
                        this.setState({directions:result});
                        this.setState({distance:result.routes[0].legs[0].distance.value});
                        this.setState({time:result.routes[0].legs[0].duration.value});
                } else {
                    console.error(`error fetching directions ${result}`);
                }
            }
        );
        }
  onClose = e => {
    this.props.onClose && this.props.onClose(e);
  };
  render() {
    if (!this.props.show) {
      return null;
    }
    const GoogleMapExample = withGoogleMap(props => (
        <GoogleMap>
        <DirectionsRenderer
            directions={this.state.directions}
        />
        {
           this.state.markeri.map( marker => {
             return <Marker {...marker} icon={{url:gasstation, anchor:new google.maps.Point(5,58)}}/>
           })
        }
    </GoogleMap>
    ));
    return (
      <div className="modal" id="modal">
        <div className="content">
            <div className="duljina-vrijeme"><div>Duljina rute: {Math.round(this.state.distance/1000)} km</div>
            <div>Vremensko trajanje: {Math.round(this.state.time/60)} min</div></div>
            <GoogleMapExample
                containerElement={<div style={{ margin:`0px`,height: `500px`, width: "500px" }} />}
                mapElement={<div style={{ margin:`0px`, height: `100%` }} />}
            />
        </div>
          <button className="zatvori-button" onClick={this.onClose}>
            ZATVORI
          </button>
      </div>
    );
  }
}
Mapa.propTypes = {
  onClose: PropTypes.func.isRequired,
  show: PropTypes.bool.isRequired,
  ulaz_lat: PropTypes.string.isRequired,
  ulaz_lng: PropTypes.string.isRequired,
  izlaz_lat: PropTypes.string.isRequired,
  izlaz_lng: PropTypes.string.isRequired
};