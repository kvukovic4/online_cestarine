import React, { Component } from "react";
import './form.css';
import Axios from 'axios';
import Mapa from './Mapa';
import { withScriptjs } from "react-google-maps";
//ikone
import car from './icons/car.png';
import moto from './icons/motorcycle.png';
import van from './icons/van.png';
import cargo from './icons/cargo.png';
import truck from './icons/truck.png';
class Form extends Component {
    constructor(){
      super();
      this.state={
        naplatne:[],
        kat:'IA',
        ulazna:'',
        izlazna:'',
        cijena: '-',
        tablica:[],
        geodata: [],
        lat_ulaz:'',
        lng_ulaz:'',
        lat_izlaz:'',
        lng_izlaz:'',
        show:false
      };
      this.onValueChangeUlazna=this.onValueChangeUlazna.bind(this);
      this.onValueChangeIzlazna=this.onValueChangeIzlazna.bind(this);
      this.radioValue=this.radioValue.bind(this);
      this.getCijena=this.getCijena.bind(this);
      this.getNaplatne=this.getNaplatne.bind(this);
      this.getGeoDataUlaz=this.getGeoDataUlaz.bind(this);
      this.getGeoDataIzlaz=this.getGeoDataIzlaz.bind(this);
      }
  componentDidMount(){
    Axios.get("http://192.168.88.198:3001/naplatne").then((response)=>{
      this.setState({naplatne:response.data})
    });
    this.setState({
      lat_ulaz:'45.74896939829503',
      lng_ulaz:'15.884319813360587',
      lat_izlaz:'45.74896939829503',
      lng_izlaz:'15.884319813360587',
      ulazna:'A1_ZAGREB',
      izlazna: ''})
    var ulaz='A1_ZAGREB';
    this.getNaplatne(ulaz);
  };
//za ulaznu naplatnu
  onValueChangeUlazna(event){
    this.setState({
      ulazna:event.target.value,
      cijena:'-',
      izlazna:'',
    })
    var ulaz=event.target.value;
    this.getNaplatne(ulaz);
    this.getGeoDataUlaz(ulaz);
  }
  //za izlaznu naplatnu
  onValueChangeIzlazna(event){
    this.setState({izlazna:event.target.value})
    var izlaz=event.target.value;
    this.getCijena(izlaz,this.state.kat);
    this.getGeoDataIzlaz(izlaz);
  }
  //za kategoriju
  radioValue(event){
    this.setState({kat:event.target.value});
    var kat=event.target.value;
    var izlaz=this.state.izlazna;
    this.getCijena(izlaz,kat);
  }
  //tablica za izabrani ulaz
  getNaplatne(ulaz){
    Axios.get(`http://192.168.88.198:3001/tablica/${ulaz}`).then((response)=>{
      this.setState({tablica:response.data[0]})
      console.log(this.state.tablica);
    });
  }
  //geodata za ulaz
  getGeoDataUlaz(ulaz){
    Axios.get(`http://192.168.88.198:3001/geodatau/${ulaz}`).then((response)=>{
      this.setState({lat_ulaz:response.data[0].geo_sirina})
      this.setState({lng_ulaz:response.data[0].geo_duzina})
    });
  }
  //geodata za izlaz
  getGeoDataIzlaz(izlaz){
    Axios.get(`http://192.168.88.198:3001/geodatai/${izlaz}`).then((response)=>{
      this.setState({lat_izlaz:response.data[0].geo_sirina})
      this.setState({lng_izlaz:response.data[0].geo_duzina})
    });
  }
  //cijena za odabrani izlaz
  getCijena(izlaz,kat){
    console.log(izlaz);
    console.log(kat);
    var tablica = this.state.tablica;
    for (var i = 0; i < tablica.length; i++){
      if (tablica[i].A === izlaz){
        if(kat === 'IA'){
          this.setState({cijena:tablica[i].B});
        }
        else if(kat === 'I'){
          this.setState({cijena:tablica[i].C});
        }
        else if(kat === 'II'){
          this.setState({cijena:tablica[i].D});
        }
        else if(kat === 'III'){
          this.setState({cijena:tablica[i].E});
        }
        else if(kat === 'IV'){
          this.setState({cijena:tablica[i].F});
        }
    }
  }
}
  //otvori modal
  showModal = e => {
    this.setState({
      show: !this.state.show
    });
  };
  render(){
    const MapLoader = withScriptjs(Mapa);
    return (
      <div className="container">
        <div className="naslov">
        <h1>Izračun cestarine</h1>
      </div>
      <form className="forma">
        {/* izbor kategorije */}
        <div className="kategorije">
        <label class="izaberi-label">Odaberi kategoriju vozila:</label>
        <div className="radio-buttoni">
        <div className="radio">
          <label>
            <input type="radio" name="kat" value="IA" defaultChecked onChange={this.radioValue}/>
            <img src={moto} alt="IA"></img>
            IA
          </label>
        </div>
        <div className="radio">
          <label>
            <input type="radio" name="kat" value="I" onChange={this.radioValue}/>
            <img src={car} alt="IA"></img>
            I
          </label>
        </div>
        <div className="radio">
          <label>
            <input type="radio" name="kat" value="II" onChange={this.radioValue}/>
            <img src={van} alt="IA"></img>
            II
          </label>
        </div>
        <div className="radio">
          <label>
            <input type="radio" name="kat" value="III" onChange={this.radioValue}/>
            <img src={cargo} alt="IA"></img>
            III
          </label>
        </div>
        <div className="radio">
          <label>
            <input type="radio" name="kat" value="IV" onChange={this.radioValue}/>
            <img src={truck} alt="IA"></img>
            IV
          </label>
        </div>
        </div>
        </div>
        {/* select za ulaznu */}
        <div className="select-ulazna">
        <label class="izaberi-label">Izaberi ulaznu naplatnu postaju:</label>  
        <select id="ulazna" value={this.state.ulazna} name="ulazna" onChange={this.onValueChangeUlazna}>
        <option value="" selected>Izaberi ulaznu naplatnu postaju: </option>
           {this.state.naplatne.map((val)=>{
            return <option key={val.ID} value={val.ID}>{val.naziv}</option>
        })}
        </select>
        </div>
        {/* select za izlaznu */}
        <div className="select-izlazna">
        <label class="izaberi-label">Izaberi izlaznu naplatnu postaju:</label>  
        <select value={this.state.izlazna} name="izlazna" onChange={this.onValueChangeIzlazna}>
           <option value="" selected>Izaberi izlaznu naplatnu postaju: </option>
           {this.state.tablica.map((val)=>{
            return <option key={val.A} value={val.A}>{val.A}</option>
        })}
        </select>
        </div>
        {/* ispis cijene */}
        <div className="div-cijena">
        <span className="labelCijena">Cijena cestarine: </span>
        <span className="cijena"> {this.state.cijena} kn</span>
        </div>
      </form>
      {/*modal*/}
      <button
        disabled={this.state.izlazna==='' || this.state.lat_ulaz === this.state.lat_izlaz}
        className="button-ruta"
        id="button-ruta"
        onClick={e => {
          this.showModal(e);
        }}
      >
          {" "}
          PRIKAŽI RUTU{" "}
        </button>
        { this.state.izlazna !=='' &&
          <MapLoader
            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCg747tpCIouQ36EemX0wgkQvGlYxsfiJc"
            loadingElement={<div style={{ height: `100%` }} />}
            onClose={this.showModal}
            show={this.state.show}
            ulaz_lat={this.state.lat_ulaz}
            ulaz_lng={this.state.lng_ulaz}
            izlaz_lat={this.state.lat_izlaz}
            izlaz_lng={this.state.lng_izlaz}
          />
        }
      </div>
    );
  }
}

export default Form;