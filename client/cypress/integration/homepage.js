describe("test", ()=>{
    it("render stranice",()=>{
        cy.visit("/");
        cy.get(".forma").should("exist");
    });

    it("select",()=>{
        cy.visit("/");
        /* ==== Generated with Cypress Studio ==== */
        cy.get('#ulazna').select('A1_OGULIN');
        cy.get('[name="izlazna"]').select('A1 - Benkovac');
        /* ==== End Cypress Studio ==== */
    });
    it("radio button",()=>{
        cy.visit("/");
        /* ==== Generated with Cypress Studio ==== */
        cy.get('[name="izlazna"]').select('A1 - Gospić');
        cy.get(':nth-child(2) > label > img').click();
        cy.get(':nth-child(2) > label > input').check();
        cy.get(':nth-child(3) > label > img').click();
        cy.get(':nth-child(3) > label > input').check();
        cy.get(':nth-child(4) > label > img').click();
        cy.get(':nth-child(4) > label > input').check();
        cy.get(':nth-child(5) > label > img').click();
        cy.get(':nth-child(5) > label > input').check();
        cy.get(':nth-child(1) > label > img').click();
        cy.get(':nth-child(1) > label > input').check();
        /* ==== End Cypress Studio ==== */
    });
    it("prikaz cijene",()=>{
        cy.visit("/");
        /* ==== Generated with Cypress Studio ==== */
        cy.get('#ulazna').select('A1_KARLOVAC');
        cy.get('[name="izlazna"]').select('A1 - Ogulin');
        cy.get(':nth-child(2) > label > img').click();
        cy.get(':nth-child(2) > label > input').check();
        cy.findAllByText("22,00");
        /* ==== End Cypress Studio ==== */
    })
    it("prikaz mape",()=>{
        cy.visit("/");
        cy.get('#ulazna').select('A1_PERUSIC');
        cy.get('.select-izlazna > select').select('A1 - Ogulin');
        cy.get('.button-ruta').click()
    })
});